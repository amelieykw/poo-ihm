'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('MainCtrl', function ($scope) {
    $scope.awesomeThings = [
      'Nationality',
      'Date de naissance',
      'Charactere'
      'Besoin'
    ];

    $scope.toto = "tutu";
  });
