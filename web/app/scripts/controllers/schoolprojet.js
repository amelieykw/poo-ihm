'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:SchoolProjetCtrl
 * @description
 * # SchoolProjetCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('SchoolProjetCtrl', function ($scope, $http) {
    $http.get("assets/schoolprojet.json")
    .success(function(data){
    	$scope.sch = data;
    })
    .error(function(error){
    	console.log(error);
    });
  });