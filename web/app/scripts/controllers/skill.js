'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:EducationCtrl
 * @description
 * # EducationCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('SkillCtrl', function ($scope, $http) {
    $http.get("assets/skill.json")
    .success(function(data){
    	$scope.skl = data;
    })
    .error(function(error){
    	console.log(error);
    });
  });
