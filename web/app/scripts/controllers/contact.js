'use strict';

/**
 * @ngdoc function
 * @name webApp.controller:EducationCtrl
 * @description
 * # EducationCtrl
 * Controller of the webApp
 */
angular.module('webApp')
  .controller('ContactCtrl', function ($scope, $http) {
    $http.get("assets/contact.json")
    .success(function(data){
    	$scope.con = data;
    })
    .error(function(error){
    	console.log(error);
    });
  });
