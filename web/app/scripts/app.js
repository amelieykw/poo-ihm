'use strict';

/**
 * @ngdoc overview
 * @name webApp
 * @description
 * # webApp
 *
 * Main module of the application.
 */
angular
  .module('webApp', [
    'ngAnimate',
    'ngCookies',
    'ngResource',
    'ngRoute',
    'ngSanitize',
    'ngTouch'
  ])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/main.html',
        controller: 'MainCtrl'
      })
       .when('/profil', {
        templateUrl: 'views/profil/profil.html',
        controller: 'ProfilCtrl'
      })
      .when('/education', {
        templateUrl: 'views/education/formation.html',
        controller: 'EducationCtrl'
      })
      .when('/schoolprojet', {
        templateUrl: 'views/schoolprojet/schoolprojet.html',
        controller: 'SchoolProjetCtrl'
      })
      .when('/skill', {
        templateUrl: 'views/skill/skill.html',
        controller: 'SkillCtrl'
      })
      .when('/about', {
        templateUrl: 'views/about.html',
        controller: 'AboutCtrl'
      })
      .when('/contact', {
        templateUrl: 'views/Contact/contact.html',
        controller: 'ContactCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });
